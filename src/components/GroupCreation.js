import React from 'react';
import { group } from '../actions/user.action';
import '../App.css';
import { bindActionCreators } from 'redux';
import {connect} from "react-redux";

class GroupCreation extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            'groupName': 'Nom du groupe',

        }
    }

    changeGroup = (event) => {
        this.setState({
            'group' : event.target.value
        })
    }

    group = () => {
        this.props.group(this.state.groupName);
    }



    render(){
        return (
            <div>
                <h1>Créer votre Groupe</h1>
                    <div className='groupCreation'>
                    <label>
                            <h2>Nom du groupe</h2>
                            <input type="text" />
                        </label>                       
                        <button onClick={this.group}>Enregistrer</button>
                    </div>
            </div>
        );
    }
}

const mapStateToProps = (state /*, ownProps*/) => {
    return {

    }
  }
  
  function mapDispatchToProps(dispatch) {
    return bindActionCreators({
      group, 
    }, dispatch)
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(GroupCreation)