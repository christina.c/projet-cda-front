import React from 'react';
import PropTypes from 'prop-types';
import { useState } from 'react';
import '../App.css';
import { loginCheck } from '../actions/user.action';

//fonction login pour connect back et front
async function loginUser(credentials) {
  return fetch('http://localhost:8000/login_check', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
      body: JSON.stringify(credentials)
    })
    .then(data => data.json())
}

export default function Login({ setToken }) {

    const [username, setUserName] = useState();
    const [password, setPassword] = useState();

    const handleSubmit = async e => {
        e.preventDefault();
        const token = await loginUser({
          username,
          password
        });
        setToken(token);
      }

  return(
    <div className='groupCreation'>
      <h1>Identifiez-vous</h1>
      <form onSubmit={handleSubmit}>
        <label>
        <h3>Identifiant</h3>
          <input type="text" onChange={e => setUserName(e.target.value)}/>
        </label>
        <label>
          <h3>Mot de passe</h3>
          <input type="password" onChange={e => setPassword(e.target.value)}/>
        </label>
        <div>
          <button onClick={loginCheck} type="submit">Connecter</button>
        </div>
      </form>
    </div>
  )
}

Login.propTypes = {
  setToken: PropTypes.func.isRequired
}
