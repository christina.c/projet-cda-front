import React from 'react';
import Footer from './Footer';
import Nav from './Nav';


class Home extends React.Component{

  render(){
    return (
      <div>
         <Nav />
          <div className='img'>
            <img src="https://media.istockphoto.com/photos/musical-instruments-picture-id894058154?k=6&m=894058154&s=612x612&w=0&h=Uti_UIlKVZA-FMB5QKOuK6QvLfUIeJ1EUNyrn7cvygA=" alt='music'></img>
          </div>
          <Footer />
        </div>
      );
  }
}

export default Home;