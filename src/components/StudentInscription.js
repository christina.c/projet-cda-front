import React from 'react';
import { student } from '../actions/user.action';
import '../App.css';
import { bindActionCreators } from 'redux';
import {connect} from "react-redux";


class StudentInscription extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            'student' : 'Prenom',
            'age' :'age',
            'level':'Niveau',
            'group':'Groupe',
        }
    }

    changeStudent = (event) => {
        this.setState({
            'student' : event.target.value
        })
    }

    student = () => {
        this.props.student(this.state.student);
        this.props.student(this.state.age);
        this.props.student(this.state.level);
        this.props.student(this.state.group);
    }

    render(){
        return (
            <div>
                <h1>Inscrivez vos élèves</h1>
                    <div className="groupCreation"> 
                        <label>
                            <h2>Prenom</h2>
                            <input type="text" />
                        </label>
                        <label>
                            <h2>age</h2>
                            <input type="text" />
                        </label>
                        <label>
                            <h2>Niveau</h2>
                            <input type="text" />
                        </label>
                        <label>
                            <h2>Groupe</h2>
                            <input type="text" />
                        </label>

                        <button onClick={this.student}>Enregistrer</button>
                    </div>
            </div>
        );
    }
}
const mapStateToProps = (state /*, ownProps*/) => {
    return {

    }
  }
  
  function mapDispatchToProps(dispatch) {
    return bindActionCreators({
      student, 
    }, dispatch)
  }

  export default connect(mapStateToProps, mapDispatchToProps)(StudentInscription)