import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import '../App.css';
import Group from '../components/Group';
import GroupCreation from '../components/GroupCreation';
import Home from '../components/Home';
import Login from '../components/Login';
import StudentInscription from '../components/StudentInscription';
import TeacherInscription from './TeacherInscription';
import StudentRegister from './StudentRegister';

class Nav extends React.Component{

  //Routeur intacte plus responsive css ok
    render(){
        return (
            <Router>
            <div className="nav">
              <nav >
                <ul>
                    <li><Link to={'/Home'} > Accueil </Link></li>  
                    <li><Link to={'/TeacherInscription'} >Inscription Professeur</Link></li>
                    <li><Link to={'/StudentRegister'} >Inscription Elèves</Link></li>
                    <li><Link to={'/Login'} > Connection </Link></li>
                    <li><Link to={'/GroupCreation'}>Groupe</Link></li>
                    <li><Link to={'/StudentInscription'}>Enregitrement élèves</Link></li>
                    <li><Link to={'/Group'} >Affichage Groupe</Link></li>
                </ul>
              </nav>
              <hr />
              <Switch>
                  <Route exact path='/' component={Home}/>
                  <Route path='/TeacherInscription' component={TeacherInscription} />
                  <Route path='/StudentRegister' component={StudentRegister} />
                  <Route path='/Login' component={Login} />
                  <Route path='/GroupCreation' component={GroupCreation} />
                  <Route path='/StudentInscription' component={StudentInscription} />
                  <Route path='/Group' component={Group} />
              </Switch>
            </div>
          </Router>
        );
    }
}
  
export default Nav