import React from 'react';
import { bindActionCreators } from 'redux';
import {connect} from "react-redux";
import { createRegister, ActionCreators} from '../actions/user.action';
import '../App.css';



class StudentRegister extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            user: {
                name:'',
                mail:'',
                phone:'',
                age:'',
                username:'',
                password:'',
                repeatPassword:''
            },
        submitted: false
    };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value,
                [name]: value,
                [name]: value,
                [name]: value,
                [name]: value,
                [name]: value,
                [name]: value
            }
        });
    }

    //REVIEW Ici j'ai ajouté la ligne 74 qui va envoyer les infos récupéré de l'état courant (user) vers une action du fichier action pour qu'elle soit ensuite envoyée à Redux
    handleSubmit = (event) => {
        console.log("handleSubmit is working");
        event.preventDefault();
        this.setState({ submitted: true });
        const { user } = this.state;

        if (user.name && user.mail && user.phone && user.age && user.username && user.password && user.repeatPassword) {
            console.log("state inside submit", this.state);
            this.props.dispatch(ActionCreators.addProfile(user))
        } else {
            console.log("Imcomplete form");
        }
        console.log("End handleSubmit");
    }

    render(){
        console.log(" FIRST STATE ->", this.state.user)
        return (
        <div>
            <h1>Inscrivez-Vous</h1>
              <div className='groupCreation'>
              <label>
                    <h2>Nom</h2>
                        <input
                            type="text"
                            name="name"
                            value={this.state.user.name}
                            onChange={this.handleChange}
                        />
                </label>
                <label>
                    <h2>Mail</h2>
                        <input
                            type="text"
                            name="mail"
                            value={this.state.user.mail}
                            onChange={this.handleChange}
                        />
                </label>
                <label>
                    <h2>Télèphone</h2>
                        <input
                            type="text"
                            name="phone"
                            value={this.state.user.phone}
                            onChange={this.handleChange}
                        />
                </label>
                <label>
                    <h2>Age</h2>
                        <input
                            type="text"
                            name="age"
                            value={this.state.user.age}
                            onChange={this.handleChange}
                        />
                </label>
                <label>
                    <h2>Identifiant</h2>
                        <input
                            type="text"
                            name="username"
                            value={this.state.user.username}
                            onChange={this.handleChange}
                        />
                </label>
                <label>
                    <h2>Mot de passe </h2>
                        <input type="text"
                            name="password"
                            value={this.state.user.password}
                           onChange={this.handleChange}
                        />
                </label>
                <label>
                    <h2>Répéter le mot de passe</h2>
                        <input type="text"
                            name="repeatPassword"
                            value={this.state.user.repeatPassword}
                            onChange={this.handleChange}
                        />
                </label>
                <div>
                    <button onClick={this.handleSubmit} type="submit">Enregistrer</button>
                </div>
            </div>                
        </div>
      );
    }
}

const mapStateToProps = (state /*, ownProps*/) => {
    return {
        user: state.user
    }
  }
  
  function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        createRegister, dispatch
    }, dispatch)
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(StudentRegister)
  