
const routes = [
    {
        'name': 'login',
        'path': '/login',
        'component': Login,
    },
    {
        'name': 'group',
        'path': '/group',
        'component': Group,
        'roles': ['ROLE_USER'],
    },
    {
        'name': 'home',
        'path': '/home',
        'component': Home,
    },
    {
        'name': 'groupCreation',
        'path': '/groupCreation',
        'component': GroupCreation,
        'roles': ['ROLE_USER'],
    },
    {
        'name': 'register',
        'path': '/register',
        'component': TeacherInscription,
    },
    {
        'name': 'student',
        'path': '/student',
        'component': StudentInscription,
        'roles': ['ROLE_USER'],
    },
 ];

