import React from 'react';
import '../App.css'


class Group extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            'groupName': 'Nom du groupe',
        }

        Group = () => {
            this.props.group(this.state.groupName);
        }
    }

    render(){
        return (
            <div className='groupeEncadrement'>
                <h2> nom du groupe</h2>
                    <div className='groupe'>
                        <p>nael/niveau 1 </p>
                        <p>natalia/niveau 1</p>
                        <p>bob/niveau 1</p>
                    </div>               
            </div>
        );
    }
}

export default Group;