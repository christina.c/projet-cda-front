
export const change = (log) => {
    return {
        type: 'CHANGE',
        value: log
    }
  };
  
  export const login = (status) => {
    return {
        type: 'LOGIN',
        value: status
    }
  };
  
  //route ok avec le back à mettre dans un fichier de config par la suite
  //route pour ce connecté
  export const loginCheck = () => {
      return async dispatch => {
        dispatch(login(true));
        const response = await fetch('http://localhost:8000/api/login_check');
        const data = await response.json();
        console.log(data);
        dispatch(change(data));
        dispatch(login(false));
      }
  };
  
  //code pour le register toutes les fonctions dont on à besoin
  function fetchJson(url, options) {
    return fetch(url, Object.assign({
        credentials: 'same-origin',
    }, options))
        .then(response => {
            return response.json();
        });
  }

  export function getRegister() {
    return fetchJson('/http://localhost:8000/api/register')
        .then(data => data.items);
  }

  export function deleteRegister(id) {
    return fetchJson(`/http://localhost:8000/api/register/${id}`, {
        method: 'DELETE'
    });
}

//Cette fonction ci-dessous sert à envoyer directement le résultat du formulaire à Redux)
export const ActionCreators = {
    addProfile: (user) => ({
      type: "ADD_USER",
      payload: { user }
    }),  
  }
  
//Ici on récupère l'utilisateur qui vient de s'inscrire, comme au dessus on l'a envoyé dans Redux, ci-dessous on se sert de getState pour le récupérer et l'assigner à la variable "register" et ensuite on a le fetch que j'ai mis dans une variable pour le voir durant les tests. A NOTER qu'il y a une erreur dans le fetch sur le JSON.parse, je pense que c'est parce que en le récupérant de Redux l'objet est déjà sous format JS donc il a pas forcément besoin de passer sous un JSON.parse mais je te laisserai regarder. Ah et aussi n'enlève pas le dispatch comme argument du return sinon il aime pas et plante une erreur (j'avoue ne pas m'être penché sur le pourquoi il n'aimait pas sorry)
export const createRegister = () => {
    return (dispatch, getState) => {
      const register = getState().homeReducer.newUser
      console.log("The register", register);
  
      let promise = fetchJson('http://localhost:8000/api/register', {
        method: 'POST',
        body: JSON.parse(register),
        headers: {
          'Content-Type': 'application/json'
        }
      });
      console.log("Promise ->", promise);
    }
  }

  //route pour les group
  export const group = () => {
    return async dispatch => {
      dispatch(login(true));
      const response = await fetch('http://localhost:8000/api/group');
      const data = await response.json();
      console.log(data);
      dispatch(change(data));
      dispatch(group(false));
    }
  };

  //route pour l'enregistrement des étudiants
  export const student = () => {
    return async dispatch => {
      dispatch(login(true));
      const response = await fetch('http://localhost:8000/api/student');
      const data = await response.json();
      console.log(data);
      dispatch(change(data));
      dispatch(student(false));
    }
  };
