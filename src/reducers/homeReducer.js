
let initialState = {
    Formulaires: {
      groupCreation: {
          name: '',
      },
      group: {
        name: '',
        student: '',
        level: '',
        link: '',
      },
      studentInscription:  {
        name: '',
        age: '',
        level: '',
        group:'',
      },
      userInscription: {
        username:'',
        mail: '',
        password: '',
        repeatPassword:'',
      }
    },
  	login: false
};

export const homeReducer = (state = initialState, action) => {
  switch (action.type) {
      case 'Login':
          return {
            ...state,
            Formulaires: action.value
          };
      case 'Logout':
          return {
            ...state,
            login: action.value
          };
        
      case "ADD_USER":
          return {
              ...state,
              newUser: action.payload.user,
          };
      default :
          return state;
  }
};

export default homeReducer;